#!/usr/bin/env bash
set -e   # set -o errexit
set -u   # set -o nounset
set -o pipefail
[ "x${DEBUG:-}" = "x" ] || set -x

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

PROJECT_ID=
ROOT_ID=
CEM_DATASET_NAME=
CEM_DEFAULT_DATASET_NAME=cem_aggregated_logs_dataset
SERVICE_ACCOUNT_NAME=cem-service-account
SERVICE_ACCOUNT_ID=cyb-cem
CEM_SINK_NAME=cem-aggregated-sink
SERVICE_ACCOUNT_CONFIG_FILE=cem_service_account.yml
CUSTOM_CEM_ROLE_CONFIG_FILE=cem_custom_role_config.yml

function ShowUsage
{
    echo "Usage: $0 -p PROJECT_ID -r ROOT_ID"
}

while getopts p:r:d:h o
do  case "$o" in
  p)  PROJECT_ID="$OPTARG";;
  r)  ROOT_ID="$OPTARG";;
  d)  CEM_DATASET_NAME="$OPTARG";;
  [?] | h) ShowUsage ; exit 1;;
  esac
done

if [[ "${PROJECT_ID}" == "PROJECT_ID" || "${ROOT_ID}" == "ROOT_ID" ]]; then
  ShowUsage
  exit 1
fi

if [[ -z "${PROJECT_ID}" || -z "${ROOT_ID}" ]]; then
  ShowUsage
  exit 1
fi

function PrintSrviceAccountError
{
cat << EOF
    ERROR: 'cem-service-account' deployment already exists.
    Please delete 'cem-service-account' deployment and all its resources and try again
EOF
}

function PrintCustomRoleError
{
cat << EOF
    ERROR: 'CustomCEMRole' custom role already exists.
    Please delete 'CustomCEMRole' custom role and try again.
EOF
}

function CheckExistingDeployment
{

    # Set cloud shell project project_id
    gcloud config set project ${PROJECT_ID} > /dev/null 2>&1

    for line in $(gcloud deployment-manager deployments list); do
        if [[ "${line}" == "cem-service-account" ]]; then
            PrintSrviceAccountError
            exit 1
        fi
    done
}

function RunDeploymentSteps
{

  # Set cloud shell project project_id
    gcloud config set project ${PROJECT_ID} > /dev/null 2>&1

    # Enable APIs
     echo "Enabling deploymentmanager, IAM ,cloudresourcemanager and bigQuery APIs..."
     gcloud services enable deploymentmanager.googleapis.com \
     cloudresourcemanager.googleapis.com \
     iam.googleapis.com \
     bigquery.googleapis.com \
     recommender.googleapis.com

    # add permissions to google serviceaccount
    echo "Adding google serviceaccount permissions..."
    gserviceaccount=$(gcloud projects get-iam-policy \
    ${PROJECT_ID} | grep -m 1 -Po 'serviceAccount:[0-9]+@cloudservices.gserviceaccount.com')

    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member ${gserviceaccount} \
    --role roles/iam.securityAdmin > /dev/null 2>&1

    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member ${gserviceaccount} \
    --role roles/iam.roleAdmin > /dev/null 2>&1

     # Create CustomCemRole and in case it exists update CustomCemRole
     gcloud iam roles create CustomCEMRole --organization=${ROOT_ID} --file=${CUSTOM_CEM_ROLE_CONFIG_FILE} > /dev/null 2>&1 \
     || gcloud iam roles update CustomCEMRole --organization=${ROOT_ID} --file=${CUSTOM_CEM_ROLE_CONFIG_FILE} --quiet

   # Create deployment
    echo "Creating CEM service account deployment..."
    gcloud deployment-manager deployments create ${SERVICE_ACCOUNT_NAME} \
    --config ${SERVICE_ACCOUNT_CONFIG_FILE}

    # Add service account to the root IAM
    echo "Adding CEM service account to organization and bind it to CustomeCemRole"
    gcloud organizations add-iam-policy-binding ${ROOT_ID} \
    --member=serviceAccount:${SERVICE_ACCOUNT_ID}@${PROJECT_ID}.iam.gserviceaccount.com \
    --role="organizations/${ROOT_ID}/roles/CustomCEMRole" \
    --condition=None

    echo "Add role to CEM service account"
    gcloud organizations add-iam-policy-binding ${ROOT_ID} \
    --member=serviceAccount:${SERVICE_ACCOUNT_ID}@${PROJECT_ID}.iam.gserviceaccount.com \
    --role roles/bigquery.jobUser \
    --condition=None

}

function CreateResources
{
    if [ -z "${CEM_DATASET_NAME}"]; then
       CEM_DATASET_NAME=$CEM_DEFAULT_DATASET_NAME
       # Create bigQuery dataset
       echo "Creating bigQuery dataset..."
       bq mk ${PROJECT_ID}:${CEM_DATASET_NAME} >/dev/null 2>&1 || echo "$CEM_DATASET_NAME already exists. "

    fi

    #Creating an aggregated sink
    echo "Creating an aggregated sink"
    gcloud logging sinks create \
    ${CEM_SINK_NAME} bigquery.googleapis.com/projects/${PROJECT_ID}/datasets/${CEM_DATASET_NAME} --include-children \
    --organization=${ROOT_ID} \
    --log-filter='protoPayload.@type ="type.googleapis.com/google.cloud.audit.AuditLog" AND NOT resource.type: k8s' \
    --quiet  > /dev/null 2>&1 || echo "${CEM_SINK_NAME} already exists"


    cemsinkservice=$(gcloud beta logging sinks describe ${CEM_SINK_NAME} --organization=${ROOT_ID} |grep -m 1 -Po 'o[0-9]+-[0-9]+' )
    #Set IAM permission to sink service account
    echo "Adding sink serviceaccount permissions..."
    gcloud organizations add-iam-policy-binding ${ROOT_ID} \
    --member serviceAccount:${cemsinkservice}@gcp-sa-logging.iam.gserviceaccount.com \
    --role roles/bigquery.dataEditor \
    --condition=None

    #Set IAM permission to sink service account
    gcloud organizations add-iam-policy-binding ${ROOT_ID} \
    --member serviceAccount:${cemsinkservice}@gcp-sa-logging.iam.gserviceaccount.com \
    --role roles/logging.logWriter \
    --condition=None

    #Update sink with the new roles
    echo "Update sink..."
    gcloud logging sinks update  ${CEM_SINK_NAME} bigquery.googleapis.com/projects/${PROJECT_ID}/datasets/${CEM_DATASET_NAME} \
    --organization=${ROOT_ID}

}

CheckExistingDeployment
RunDeploymentSteps
CreateResources
